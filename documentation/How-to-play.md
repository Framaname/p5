How to play this game?
====

Choose level
----
You can choose level by filling the **field** in the top left with the **level number**.

Commands
----
Jump: **UP ARROW**

Left: **LEFT ARROW**

Right: **RIGHT ARROW**

Retry: **SHIFT**

Increase speed: **!**

Decrease speed: **:**
